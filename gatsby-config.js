module.exports = {
    siteMetadata: {
        heroTitle: "Mapping Software Documentation",
    },
    plugins: [
        {
            resolve: "@jorsek/gatsby-theme-easydita-jsk",
            options: {
                CONTENT_RUNTIME_REMOTE: false,
                CONTENT_BUILDTIME_REMOTE: false,
                CONTENT_BUILDTIME_STATIC: true,
                CONTENT_STATIC_DIR: "ezd_content",
                CONTENT_ORG: "",
                CONTENT_ROOTMAP: "",
                CONTENT_TOKEN: "",
                CONTENT_TRANSFORMS: [
                    {
                        transformToPage(input, section) {
                            const crumb_copy = [...input.breadcrumbs];
                            // API includes the current page at the end of the
                            // breadcrumbs array, but we never want that.
                            crumb_copy.pop();

                            // API also includes an empty breadcrumb at the beginning of the
                            // breadcrumbs array - let's clean that up, too.
                            crumb_copy.shift();

                            // instead of redirecting from the top level section page to the first child,
                            // we're rewriting any link to that section page, (also see below in transformToSection)
                            if (crumb_copy.length > 0) {
                                crumb_copy[0].href = section.href;
                            }
                            return {
                                ...input,
                                breadcrumbs: crumb_copy,
                            };
                        },
                    },
                    {
                        transformToSection(input) {
                            // instead of redirecting from the top level section page to the first child,
                            // we're rewriting any link to that section page, (also see above in transformToPage)
                            if (input.children && input.children[0] && input.children[0].href) {
                                input.href = input.children[0].href;
                            }
                            return input;
                        },
                    },
                ],
            },
        },
    ],
};
