import React from "react";
import { Theme, makeStyles, Typography } from "@material-ui/core";
import { SocialSharing } from "@jorsek/static-portal-components";
import RSSIcon from "@material-ui/icons/RssFeed";
import blueGrey from "@material-ui/core/colors/blueGrey";
import { FacebookIcon, TwitterIcon } from "react-share";

const withStyles = makeStyles((theme: Theme) => {
    return {
        footer_item: {
            "marginLeft": theme.spacing(10),
            "& a": {
                "textDecoration": "none",
                "color": theme.palette.grey[300],
                "&:hover": {
                    color: theme.palette.grey[50],
                },
            },
            [theme.breakpoints.down("sm")]: {
                "margin": "0 auto",
                "& a": {
                    // marginRight: theme.spacing(3)
                },
                "& svg": {
                    height: theme.spacing(5),
                    width: theme.spacing(5),
                },
            },
        },
        footer_outer: {
            backgroundColor: "#332F30",
            color: theme.palette.grey[50],
            justifyContent: "center",
            display: "flex",
        },
        footer_inner: {
            maxWidth: 1200,
            display: "flex",
            width: "100%",
            padding: theme.spacing(3),
        },
    };
});

const Footer = (props: any) => {
    const classes = withStyles(props);
    const getCurrentYear = () => new Date().getFullYear();

    return (
        <div className={classes.footer_outer}>
            <div className={classes.footer_inner}>
                <div className={classes.footer_item}>
                    © {getCurrentYear()} NavVis | <a href="https://www.navvis.com/imprint">Imprint</a> |
                    <a href="https://www.navvis.com/privacy-policy">Privacy Policy</a>
                </div>
                <div style={{ flexGrow: 1 }} />
                <div className={classes.footer_item}>
                </div>
            </div>
        </div>
    );
};

export default Footer;
