import React from "react";
import { Theme, makeStyles } from "@material-ui/core";
import { Link } from "gatsby";

const withStyles = makeStyles((theme: Theme) => {
    return {
        logo: {
            marginRight: theme.spacing(1),
            width: 200,
        },
    };
});

const Logo = (props: any) => {
    const classes = withStyles(props);

    return (
        <Link to="/">
            <img className={classes.logo} src={require("../../../assets/logo.png")} />
        </Link>
    );
};

export default Logo;
