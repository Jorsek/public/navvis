import React from "react";
import { Typography, makeStyles, Theme, Card, CardActionArea, CardContent } from "@material-ui/core";
import { cyan, grey } from "@material-ui/core/colors";
import { Link } from "gatsby";
import { IHomePageProps } from "@jorsek/ezd-client/dist/src/Core";
import HeroImage from "@jorsek/gatsby-theme-easydita-core/src/components/HeroImage";
import { useSiteMetadata, useGroup } from "@jorsek/gatsby-theme-easydita-core/src/utils/hooks";

const withStyles = makeStyles((theme: Theme) => {
    return {
        content: {
            display: "flex",
            paddingTop: theme.spacing(5),
            paddingBottom: theme.spacing(5),
            width: "100%",
            maxWidth: theme.spacing(180),
            alignSelf: "center",
            [theme.breakpoints.down("sm")]:{
                maxWidth: theme.spacing(44)
            }
        },
        ezd_hero_text: {
            margin: theme.spacing(2),
            textAlign: "left",
            width: theme.spacing(75),
            [theme.breakpoints.down("sm")]: {
                width: theme.spacing(37.5),
                fontSize: "2.0rem",
                textAlign: "center",
                margin: "auto"
            },
        },
        heroSubtitle: {
            fontFamily: "Lato", 
            fontWeight: 100,
            textAlign: "left",
            margin: theme.spacing(2),
            [theme.breakpoints.down("sm")]: {
                fontSize: "1.25rem",
                textAlign: "center"
            },
        },
        products: {
            flex: "2 2 auto",
            marginRight: theme.spacing(2),
            display: "flex",
            flexWrap: "wrap",
        },
        feature: {
            [theme.breakpoints.down("sm")]: {
                width: "100%"
            }
        },
        product_card: {
            margin: theme.spacing(1.5, 1.25),
            width: "100%",
            height: "100%",
            backgroundColor: theme.palette.primary.main,
            color: grey[100],
            textAlign: "center",
        },
        productCardText: {
            textAlign: "center"
        },
        sidebar: {
            flex: "1 1 auto",
            display: "flex",
            flexDirection: "column",
            [theme.breakpoints.down("md")]: {
                visibility: "hidden"
            },
        },
        media: {
            width: "30%",
            display: "block",
            margin: "auto",
        },
        link: {
            textDecoration: "none",
            margin: theme.spacing(1.5, 1.25),
            width: theme.spacing(39.5),
            height: theme.spacing(10.5)
        },
    };
});

const IndexPage = (_props: IHomePageProps) => {
    const styles = withStyles({});
    const metadata = useSiteMetadata();
    const mainGroup = useGroup("main");
    return (
        <>
            <div className={styles.content}>
                <div className={styles.feature}>
                    <Typography align="center" variant="h2" className={styles.ezd_hero_text}>
                        {metadata.heroTitle}
                    </Typography>
                    <Typography
                        color="inherit"
                        variant="h4"
                        align="center"
                        className={styles.heroSubtitle}
                    >
                        {metadata.heroSubtitle} 
                    </Typography>
                    <div className={styles.products}>
                        {mainGroup.children.map(card => (
                            <Link className={styles.link} key={card.href} to={`/content/${card.href}`}>
                                <Card className={styles.product_card}>
                                    <CardActionArea style={{ height: "100%" }}>
                                        <CardContent>
                                            {card.thumbnail ? (
                                                <img className={styles.media} src={card.thumbnail} />
                                            ) : null}
                                            <Typography className={styles.productCardText} align="left" variant="h6" component="h2" color="inherit">
                                                {card.title}
                                            </Typography>
                                            <Typography align="left" color="inherit">
                                                {card.shortdesc}
                                            </Typography>
                                        </CardContent>
                                    </CardActionArea>
                                </Card>
                            </Link>
                        ))}
                    </div>
                </div>
                <div className={styles.sidebar}>
                    <HeroImage />
                </div>
            </div>
        </>
    );
};

export default IndexPage;
