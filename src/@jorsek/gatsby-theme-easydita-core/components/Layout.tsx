import React from "react";
import { AppBar, Toolbar, Typography, Theme, Button, Tabs, Tab, Drawer, Link as MUILink } from "@material-ui/core";
import { makeStyles, useTheme } from "@material-ui/styles";
import { navigate, Link } from "gatsby";
import { useLocation } from "@reach/router";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import Logo from "@jorsek/gatsby-theme-easydita-core/src/components/Logo";
import { useGroup } from "@jorsek/gatsby-theme-easydita-core/src/utils/hooks";
import MenuIcon from "@material-ui/icons/Menu";
import Footer from "@jorsek/gatsby-theme-easydita-jsk/src/components/Footer";

const withStyles = makeStyles((theme: Theme) => ({
    content: {
        display: "flex",
        flexDirection: "column",
        height: "100%",
        minHeight: "100vh",
        paddingTop: theme.spacing(20),
        backgroundColor: theme.palette.background.default,
        [theme.breakpoints.down("sm")]: {
            paddingTop: theme.spacing(5),
        },
    },
    header: {
        backgroundColor: "#332F30",
    },

    top_nav_wrapper: {
        backgroundColor: "#332F30",
        color: "#F8F7FA",
        height: theme.spacing(6.25),
        [theme.breakpoints.down("sm")]: {
            display: "none",
        },
    },
    top_nav: {
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2),
        display: "flex",
        alignItems: "center",
        margin: "0 auto",
    },
    tabRoot: {
        minWidth: "inherit",
        fontSize: "1.0rem",
    },

    toolbar: {
        alignSelf: "flex-start",
        maxWidth: theme.spacing(205),
        width: "100%",
        paddingRight: theme.spacing(2),
        [theme.breakpoints.down("sm")]: {
            "paddingRight": theme.spacing(7),
            "& > a": {
                width: "100%",
            },
            "& .gatsby-image-wrapper": {
                display: "flex !important",
                margin: "auto",
            },
        },
    },
    drawerPaper: {
        width: "40%",
    },
    drawer_icon: {
        marginRight: theme.spacing(2),
        color: "#F8F7FA",
        border: `1px solid ${theme.palette.grey[300]}`,
    },
    drawer_contents: {
        "marginTop": theme.spacing(2),
        "& .MuiTabs-flexContainer": {
            flexDirection: "column",
        },
        "& .MuiTab-wrapper": {
            textTransform: "capitalize",
            textAlign: "left",
            display: "block",
        },
    },
}));

const Layout: React.FunctionComponent = (props) => {
    const sections = useGroup("main")?.children;
    const loc = useLocation();
    const [drawerOpen, setDrawerOpen] = React.useState(false);
    const current_path = loc.pathname.replace("/content/", "").split("/")[0];
    const theme: Theme = useTheme();
    const isSmall = useMediaQuery(theme.breakpoints.down("sm"));

    const classes = withStyles(props);
    return (
        <div className={classes.content}>
            <AppBar color="inherit" position="fixed" className={classes.header}>
                <Toolbar className={classes.toolbar}>
                    {isSmall ? (
                        <>
                            <MenuIcon className={classes.drawer_icon} onClick={() => setDrawerOpen(true)} />
                            <Logo />
                        </>
                    ) : (
                        <>
                            <Logo />
                            <div style={{ flexGrow: 1 }} />
                        </>
                    )}
                </Toolbar>
                <div className={classes.top_nav_wrapper}>
                    {isSmall ? (
                        <Drawer
                            open={drawerOpen}
                            onClose={() => setDrawerOpen(false)}
                            classes={{ paper: classes.drawerPaper }}
                        >
                            <div className={classes.drawer_contents}>
                                <div>
                                    <Tabs value={current_path}>
                                        {(sections || []).map((section) => (
                                            <Tab
                                                key={section.href}
                                                value={section.href.split("/")[0]}
                                                label={section.title}
                                                classes={{ root: classes.tabRoot }}
                                                onClick={(evt) => {
                                                    evt.preventDefault();
                                                    navigate(`/content/${section.href}`);
                                                    setDrawerOpen(false);
                                                }}
                                            />
                                        ))}
                                    </Tabs>
                                </div>
                                <div style={{ flexGrow: 1 }} />
                            </div>
                        </Drawer>
                    ) : (
                        <div className={classes.top_nav}>
                            <div style={{ flex: "8 1 auto" }}>
                                {(sections || []).map((section) => (
                                    <Link style={{ margin: 12 }} key={section.href} to={`/content/${section.href}`}>
                                        <MUILink>{section.title}</MUILink>
                                    </Link>
                                ))}
                            </div>
                        </div>
                    )}
                </div>
            </AppBar>
            {props.children}
            <div style={{ flexGrow: 1 }} />
            <Footer />
        </div>
    );
};

export default Layout;
