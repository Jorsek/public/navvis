import React from "react";
import { Theme, makeStyles } from "@material-ui/core";

const withStyles = makeStyles((theme: Theme) => {
    return {
        hero_image: {
            marginRight: theme.spacing(1),
            width: 350,
        },
    };
});

const HeroImage = (props: any) => {
    const classes = withStyles(props);
    return <img className={classes.hero_image} src={require("../../../assets/hero.png")} />;
};

export default HeroImage;
