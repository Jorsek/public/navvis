import React from "react";
import { Link } from "gatsby";
import { makeStyles, useTheme } from "@material-ui/styles";
import { Theme, Drawer, CardContent, Link as MUILink, Toolbar } from "@material-ui/core";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { Breadcrumbs, NavTree } from "@jorsek/static-portal-components";
import MenuIcon from "@material-ui/icons/Menu";

import Content from "@jorsek/gatsby-theme-easydita-core/src/components/Content";

const withStyles = makeStyles((theme: Theme) => ({
    content: {
        flex: "1"
    },
    navbar: {
        flexBasis: theme.spacing(35),
        overflow: "auto",
        backgroundColor: theme.palette.background.default,
        borderRight: `1px solid ${theme.palette.grey[300]}`,
        height: "100%",
        paddingLeft: theme.spacing(1),
        "& p":{
            fontSize: "1.0rem"
        }
    },
    page_contents: {
        width: "100%",
        overflowY: "auto",
        display: "flex",
        flex: "1",
        [theme.breakpoints.down("md")] :{
            marginTop: theme.spacing(6)
        }
    },
    page_contents_container: {
        "overflow": "auto",
        "@global": {
            "*": {
                fontSize: "1.125rem",
                maxWidth: "40em",
                lineHeight: "2em",
            },
            "section": {
                marginTop: "40px",
            },
            "table": {
                "marginTop": "40px",
                "& th": {
                    border: "1px solid #989898",
                    textAlign: "left",
                    paddingLeft: "30px",
                },
                "& td": {
                    border: "1px solid #989898",
                    padding: "0 30px",
                },
                "border": "1px solid #989898",
                "borderCollapse": "collapse",
            },
            "ul": {
                "& li": {
                    "& p": {
                        display: "inline",
                    },
                },
            },
        },
    },
    upper_contents_container: {
        display: "flex",
    },
    drawerPaper: {
        width: "40%",
    },
    drawer_contents: {
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2),
    },
    drawer_icon: {
        marginRight: theme.spacing(2),
    },
    navToolbar: {
        "& li": {
            "& a": {
                "& :hover": {
                    color: theme.palette.secondary.main,
                },
                "color": theme.palette.primary.dark,
            },
        },
        "backgroundColor": theme.palette.background.default,
        "color": theme.palette.primary.dark,
        "minHeight": theme.spacing(6),
        "padding": "0",
    },
    outer_navbar: {
        flex: "0 0 280px",
        position: "sticky",
        top: "0",
        [theme.breakpoints.down("md")]:{
           display: "none"
        }
    },
    mainContent: {
        flex: "3",
        backgroundColor: theme.palette.background.default,
        padding: "25px 50px",
        margin: "25px",
        [theme.breakpoints.down("md")]:{
            maxWidth: theme.spacing(113),
            marginTop: theme.spacing(-4),
            marginLeft: 0,
            paddingTop: 0
        },
        [theme.breakpoints.down("sm")]:{
            padding: "0 0"
        }
    },
}));

export default ({ page: { href, breadcrumbs }, navtree, content, loading }: Parameters<typeof Content>[0]) => {
    const [drawerOpen, setDrawerOpen] = React.useState(false);

    const classes = withStyles({});

    const theme: Theme = useTheme();
    const isMedium = useMediaQuery(theme.breakpoints.down("md"));

    let breadcrumbComponent = (
        <Breadcrumbs
            breadcrumbs={breadcrumbs}
            pathPrefix="/content/"
            path={href}
            renderLink={(crumb, { visualPath }) => {
                return (
                    <Link to={`${visualPath}`}>
                        <MUILink>{crumb.title}</MUILink>
                    </Link>
                );
            }}
        />
    );

    if (isMedium) {
        breadcrumbComponent = (
            <>
                <MenuIcon className={classes.drawer_icon} color="action" onClick={() => setDrawerOpen(true)} />
            </>
        );
    }

    const renderSidebar = ({ tree, path }) => (
        <div>{tree ? <NavTree data={tree} path={path} pathPrefix="/content/" /> : "Loading..."}</div>
    );

    const sidebar = renderSidebar({
        path: href,
        tree: navtree?.children,
    });

    return (
        <div className={classes.page_contents}>
            <div className={classes.outer_navbar}>
                {isMedium ? (
                    <Drawer
                        open={drawerOpen}
                        onClose={() => setDrawerOpen(false)}
                        classes={{ paper: classes.drawerPaper }}
                    >
                        <div className={classes.drawer_contents}>{sidebar}</div>
                    </Drawer>
                ) : (
                    <div className={classes.navbar}>
                        <CardContent>{sidebar}</CardContent>
                    </div>
                )}
            </div>
            <div className={classes.content}>
                {loading ? (
                    <span>Loading...</span>
                ) : (
                    <div className={classes.upper_contents_container}>
                        <div className={classes.mainContent}>
                            <CardContent>
                                {
                                    <Toolbar className={classes.navToolbar}>
                                        {breadcrumbComponent}
                                        <div style={{ flexGrow: 1 }} />
                                    </Toolbar>
                                }
                                <div className={classes.page_contents_container}>{content}</div>
                            </CardContent>
                        </div>
                    </div>
                )}
            </div>
        </div>
    );
};
