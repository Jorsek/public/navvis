import { createMuiTheme } from "@material-ui/core/styles";
export const theme = createMuiTheme({
    palette: {
        primary: {
            light: "#6fcafb",
            main: "#3399C8",
            dark: "#006b97",
        },
        secondary: {
            light: "#ffd759",
            main: "#F5A623",
            dark: "#bd7700",
        },
        text: {
            primary: "#353436",
        },
        background: {
            default: "##fff",
            paper: "#F8F7FA",
        },
    },
    typography: {
        fontFamily: "Lato",
    },
    shape: {
        borderRadius: 2,
    },
});
